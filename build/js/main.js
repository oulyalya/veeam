(function () {
  'use strict';

  const expertsSwiper = new window.Swiper('.experts__slider', {
    direction: 'horizontal',
    slidesPerGroup: 1,
    slidesPerView: 'auto',
    effect: 'slide',
    initialSlide: 0,
    simulateTouch: true,
    spaceBetween: 33,
    watchOverflow: true,

    lazy: {
      loadPrevNext: true,
    },

    breakpoints: {
      1024: {
        spaceBetween: 33,
      },

      768: {
        spaceBetween: 10,

      },

      320: {
        spaceBetween: 5,
      },
    },
  });

  expertsSwiper.init();
})();

(function () {
  'use strict';

  const body = document.querySelector('body');
  const overlay = document.querySelector('.overlay');
  const videoPopup = document.querySelector('.popup');
  const video = document.querySelector('.popup iframe');
  const videoBtn = document.querySelector('.video__button--play');
  const closeBtn = videoPopup.querySelector('.popup__close-btn');

  function pauseVideo() {
    if (video !== null) video.contentWindow.postMessage('{"event":"command","func":"stopVideo","args":""}', '*');
  }

  function showOverlay() {
    overlay.classList.add('overlay--shown');
  }

  function hideOverlay() {
    overlay.classList.remove('overlay--shown');
  }

  function showPopup() {
    videoPopup.classList.add('popup--shown');
  }

  function hidePopup() {
    videoPopup.classList.remove('popup--shown');
  }


  function showVideo(evt) {
    showOverlay();
    showPopup();

    if (body.offsetHeight > window.innerHeight) {
      body.classList.add('js-no-scroll');
    }

    closeBtn.addEventListener('click', hideVideo);
  }

  function hideVideo(evt) {
    pauseVideo();
    hidePopup();
    hideOverlay();

    if (body.offsetHeight > window.innerHeight) {
      body.classList.remove('js-no-scroll');
    }

    closeBtn.removeEventListener('click', hideVideo);
    videoBtn.addEventListener('click', showVideo);
  }

  videoBtn.addEventListener('click', showVideo);
})();

(function () {
  'use strict';

  const wow = new window.WOW(
    {
      boxClass: 'wow',
      animateClass: 'animated',
      offset: 0,
      mobile: true,
      live: true
    }
  );

  wow.init();
})();
