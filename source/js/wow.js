(function () {
  'use strict';

  const wow = new window.WOW(
    {
      boxClass: 'wow',
      animateClass: 'animated',
      offset: 0,
      mobile: true,
      live: true
    }
  );

  wow.init();
})();
