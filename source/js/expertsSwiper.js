(function () {
  'use strict';

  const expertsSwiper = new window.Swiper('.experts__slider', {
    direction: 'horizontal',
    slidesPerGroup: 1,
    slidesPerView: 'auto',
    effect: 'slide',
    initialSlide: 0,
    simulateTouch: true,
    spaceBetween: 33,
    watchOverflow: true,

    lazy: {
      loadPrevNext: true,
    },

    breakpoints: {
      1024: {
        spaceBetween: 33,
      },

      768: {
        spaceBetween: 10,

      },

      320: {
        spaceBetween: 5,
      },
    },
  });

  expertsSwiper.init();
})();
